# Second signature - gendexoperatorordertx  
generator an operator dex order tx, support operator config, and must be signed by operator before sumiting. 

**function**  
1、suppose one rdio corresponds to one order
2、DEX can help pay for fee of miner

**Parameters**  
gendexoperatorordertx "sender" "order_type" "order_side" "symbol:coins:unit" "symbol:assets:unit" price dex_id "open_mode" taker_fee_ratio maker_fee_ratio "[symbol:fee:unit]" "[symbol:operator_tx_fee:unit]" "[memo]"  

`sender`                          (string required)  the tx sender's address  

`order_type`                  (string required) order type, must be in (LIMIT_PRICE, MARKET_PRICE)      

`order_side`                  (string required) order side, must be in (BUY, SELL)  

`symbol:coins:unit`   (string:numeric:string, required)  the coins(money) of order, coins=0 if not market buy order,
default symbol is WUSD, default unit is sawi.   

`symbol:assets:unit`  (string:numeric:string, required)  the assets of order, assets=0 if market buy orderdefault symbol is WICC, default unit is sawi.  

`price`                             (numeric, required) expected price of order, boost 100000000  

`dex_id`                          (numeric, required) Decentralized Exchange(DEX) ID    

`open_mode`                    (string, required) indicate the order is PUBLIC or PRIVATE  

`taker_fee_ratio`       (numeric, required)   taker fee ratio config by operator, boost 100000000  

`maker_fee_ratio`        (numeric, required) maker fee ratio config by operator, boost 100000000  

`symbol:fee:unit`        (string:numeric:string, optional) fee pay by tx user to miner, default is WICC:10000:sawi  

`symbol:operator_tx_fee:unit`    (string:numeric:string, optional) tx fee pay by operator to miner,symbol must equal to fee, default is WICC:0:sawi  

`memo`                              (string, optional) memo  

**Result**  
`txid`    (string)  The transaction id.     

**Examples**  
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind gendexoperatorordertx 0-1 LIMIT_PRICE SELL WUSD:0 WICC:20000000000:sawi 100000000000 0 PUBLIC 8000 4000

// Respond
{
    "rawtx" : "5b0180b65e0200010457494343858c20010204574943430457555344c9bfde8f000081f3c2dacf0000019e20bd4002080300000000"
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "gendexoperatorordertx", "params": ["10-3", "LIMIT_PRICE", "BUY", "WUSD:0", "WICC:2000000000:sawi", 100000000, 0, "PUBLIC", 80000, 40000
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```
--- 
