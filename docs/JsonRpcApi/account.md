 
#### API's for account usage

<br>

## `coind listaddr` 

**View all addresses and balances in the node wallet.**

return Array containing address, balance, haveminerkey, regid information.

**Result:**

```json

  {
        "addr" : "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4",
        "regid" : "0-1",
        "regid_mature" : true,
        "received_votes" : 0,
        "tokens" : {
            "WICC" : {
                "free_amount" : 20790000000000000,
                "staked_amount" : 0,
                "frozen_amount" : 0,
                "voted_amount" : 210000000000000
            }
        },
        "hasminerkey" : false
    },
```

**Examples:**

```
> ./coind listaddr
```

**As json rpc call**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "listaddr", "params": [] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---


## `coind getnewaddr`

**get a new address**

**Arguments:**

1. "IsMiner" (bool, optional) If true, it creates two sets of key-pairs: one for mining and another for receiving miner fees.

**Result:**

**Examples:**

```
> ./coind getnewaddr
```

**As json rpc**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getnewaddr", "params": [] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---



## `coind submitaccountregistertx`
  
   **register local account public key to get its RegId**
   
   submitaccountregistertx "addr" ("fee")
   
   **Arguments:**
   
   1. addr: (string, required)
   2. fee: (numeric, optional) pay tx fees to miner
   
   **Result:**
   
   "txid": (string)
   
   **Examples:**
   
``` 
  > ./coind submitaccountregistertx n2dha9w3bz2HPVQzoGKda3Cgt5p5Tgv6oj 100000
```   
   **As json rpc call**
   
```
   > curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitaccountregistertx", "params": [n2dha9w3bz2HPVQzoGKda3Cgt5p5Tgv6oj 100000 ] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

<br>

---

## `coind getaccountinfo`

get account information

**Arguments:**

1. "addr": (string, required) account base58 addressReturns account details.

**Returns**

`Address` Ordinary account/contract account address

`KeyID`   sha256((public key))

`RegID`   registered id

`regid_mature` the maturlty of regid, for check if regid permanent availability 

```
true : maturity
false: immaturate
```

`owner_pubkey` public key(Ordinary account only）

`miner_pubkey` Miner Key (ignored)

`tokens`  currency balance list, maybe contain 0 or more of WICC WUSD WGRT
```
`free_amount`   discretionary amounts, unit:sawi
`staked_amount` pledged amounts, unit:sawi
`frozen_amount` frozen amounts, unit:sawi
`voted_amount`  voted amounts, unit:sawi 
```

`received_votes` received amounts of vote

`vote_list` List of votes issued

`cdp_list` list of cdp

`perm` contain 10 bits of binary 

    `NULL_ACCOUNT_PERM`   = 0,         // no perm at all w/ the account, even for smart contract account
    `PERM_SEND_COIN`      = (1 << 0 ), //recv_coin is always allowed
    `PERM_STAKE_COIN`     = (1 << 1 ), //when input is negative, it means unstake
    `PERM_SEND_VOTE`      = (1 << 2 ), //send or revoke votes to others
    `PERM_SEND_UTXO`      = (1 << 3 ), //recv utox is always allowed
    `PERM_CONTRACT`       = (1 << 4 ), //perm of deploy|invoke contract
    `PERM_PROPOSE`        = (1 << 5 ), //DeGov propose
    `PERM_MINE_BLOCK`     = (1 << 6 ), //elected BP can mine blocks, mostly used to disable the perm when set zero
    `PERM_FEED_PRICE`     = (1 << 7 ), //Feed price, mostly used to disable the perm when set zero
    `PERM_DEX`            = (1 << 8 ), //freeze | unfreeze
    `PERM_CDP`            = (1 << 9),  //pledge | unpledge
    
`onchain`  the status of public key submit in public chain   

`in_wallet` the status of public key exit in local  

`pubkey_registered`  the status of pubkey register 



**Result:**

```json
{
 "address" : "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4",
    "keyid" : "079b9296a00a2b655787fa90e66ec3cde4bf1c8c",
    "regid" : "0-1",
    "regid_mature" : true,
    "owner_pubkey" : "036c5397f3227a1e209952829d249b7ad0f615e43b763ac15e3a6f52627a10df21",
    "miner_pubkey" : "",
    "perms" : "1111111111",
    "tokens" : {
        "WICC" : {
            "free_amount" : 20790000000000000,
            "staked_amount" : 0,
            "frozen_amount" : 0,
            "voted_amount" : 210000000000000,
            "pledged_amount" : 0,
            "total_amount" : 21000000000000000
        }
    },
    "received_votes" : 0,
    "vote_list" : [
        {
            "candidate_uid" : {
                "id_type" : "PubKey",
                "id" : "03ff9fb0c58b6097bc944592faee68fbdb2d1c5cd901f6eae9198bd8b31a1e6f5e"
            },
            "voted_bcoins" : 210000000000000
        }
    ],
    "onchain" : true,
    "in_wallet" : false,
    "pubkey_registered" : true,
    "cdp_list" : [
    ]
}
```

**Examples:**

```
> ./coind getaccountinfo "WT52jPi8DhHUC85MPYK8y8Ajs8J7CshgaB"
```

**As json rpc call**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getaccountinfo", "params": ["WT52jPi8DhHUC85MPYK8y8Ajs8J7CshgaB"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---


## `coind dumpprivkey`

Reveals the private key by address

**Parameters**

`address` Common account address in this wallet

**Returns**

`privatekey` private key（WIF format）

`minerkey`   miner identification (ignored)

**Example**

As a json rpc call

```

> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "dumpprivkey", "params": ["address"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

Results

```json
{
  "result":
  {
    "privkey":"PibUXsLW6jyixtuxUTDhwDm6q5FDXkd6JAevDmy5soLFPxxxxxx",
    "minerkey":" "
  },
  "error": null,
  "id": "curltext"
}
```

---


## `coind importprivkey`

import privkey（from method `dumpprivkey`）to wallet

**Requirement**

the wallet must be unlocked.

**Parameters**

`privkey` common account private key（WIF format）

**Returns**

`imorpt key address` common account address

**Example**
```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"importprivkey","params":["PibUXsLW6jyixtuxUTDhwDm6q5FDXkd6JAevDmy5soLFPxxxxxx"]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
  "result":
  {
    "imorpt key address":"WU43D2KLnj1msgrpBeJ5xukR1toD7WTjpS"
  },
  "error": null,
  "id": "curltext"
}

```

---


## `coind validateaddr`

Check common account or contract account address is valid or not

**Parameters**

`address` common account/contract account address/regid

**Returns**

`is_valid` = `true` valid

`is_valid` = `false` invalid

**Example**
```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"validateaddr","params":["wVCfTim2m7F6u5qt9SUixUngbqkpGMoSL6"]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
  "result":
  {
    "is_valid" : true,
    "addr" : "wVNb2zd9BsNqJBQgWY4By7C2M4U88AmvLt"
  },
  "error": null,
  "id": "curltext"
}
```

---

## `submitcoinstaketx`

Stake coin for example to become a price feeder you have to stake at least 210,000 coins

submitcoinstaketx "addr" "coin_symbol" "coin_amount" ["symbol:fee:unit"]

stake fcoins

**Arguments:**

1. `sender`:           (string, required) the tx sender's address
2. `coins_to_stake`:  (symbol:amount:unit, required) coins to stake or unstake to the account, default symbol=WICC, default unit=sawi
3. `action`:          (string, optional) action for staking coins, must be (STAKE | UNSTAKE), default is STAKE
4. `fees`:             (symbol:amount:unit, optional) fees paid to miner, default is WICC:10000:sawi
**Result:**

`txid`               (string) The transaction id.

**Examples:**
```json
> ./coind submitcoinstaketx "10-1" "WICC:21000:wi" "STAKE" "WICC:0.1:wi"

As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitcoinstaketx", "params": ["10-1", "WICC:21000:wi", "STAKE", "WICC:0.1:wi"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

<br>

---

[1]:	./tx.md/#submittx
