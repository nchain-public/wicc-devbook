<extoc></extoc>
# Waykichain.conf Parameter Description
WaykiChain nodes support `privatenet`、`testnet`、`mainnet`

Depending on `Waykichain.conf` to configure different nettype to run blockchain

##### Configuration
|       name        | value(example) |                         memo                          |
|:-----------------|:--------------:|:--------------------------------------------------------:|
| nettype			|   `main` &#124; `test` &#124; `regtest`   |    `Main network: main` &#124; `Test network: test` &#124; `Private network: regtest` |
|      rpcuser      |   wiccuser   | RPC username                    							|
|    rpcpassword    |    wicc1000    |                       RPC password                      		|
|       debug       | `INFO`&nbsp;&#124;&nbsp;`ERROR`&nbsp;&#124;&nbsp;`DEBUG`&nbsp;&#124;&nbsp;`WASM`      | debug level: It can appear alone or in combination   |
| logprinttoconsole |       0        |           log print to console; 1:true,0:false           |
|   logtimestamps   |       1        |           log print timestamps; 1:true,0:false           |
|  logprinttofile   |       1        |            log print to file; 1:true,0:false             |
| logprintfileline  |       1        |         log print with fileline; 1:true,0:false          |
|      rpcserver    |       1        |  RPC service enable configuration, no configuration is enabled by default, 0: no, 1: yes  					|
|      listen       |       1        |                        Whether the RPC service listens                                  |
|      rpcport      |      6968      |                         RPC service port number                        |
|    rpcallowip     |  0.0.0.0/0     | Specific configuration of IPs that allow access to RPC services, you can set any ip address |
|  disablesafemode  |       1        | Whether to close secure mode, when a fork occurs, the closed secure mode RPC interface will not be available                                      |
|     genblock      |       0        | 0: no block, 1: block(It only makes sense if the current node is a BP node) |
|   genblocklimit   |    10000000000 |The block node stops after it reaches the specified height, and only the RegTest network is used for debugging. The rest of the network does not need to be configured.   |
|    rpcthreads     |       8        |              Number of RPC concurrent threads requested                                            |
|  rpcwhitelistcmd  |  names of rpc  |  All interface are disabled except some set in rpcwhitelistcmd. such as: rpcwhitelistcmd=getinfo then getinfo is vaild    |
|  rpcblacklistcmd  |  names of rpc  | All interface are vaild except some set in rpcblacklistcmd. such as: rpcblacklistcmd=getinfo then getinfo is disabled   |
| contracts_console | 1             |  Print wasm contract logs to console(default: 0)  |

**action:** 

1. if set rpcwhitelistcmd and rpcblacklistcmd in same time, then whitelist first in case of conflict.  
2. if there need set multiple interface in whitelist or blacklist, please set multi-line `rpcwhitelistcmd=$name`. such as:  
```
rpcwhitelistcmd=getinfo   
rpcwhitelistcmd=stop  
```
---

**example**

## MainNet

```
#nettype=main|test|regtest
nettype=main
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
debuglogfile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

---

## TestNet

```
#nettype=main|test|regtest
nettype=test
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
debuglogfile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

---

## RegTest

```
#nettype=main|test|regtest
nettype=regtest
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
debuglogfile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

<br>

---
