# Dex Registration  
---
##  submitdexoperatorregtx  

register a dex operator

submitdexoperatorregtx  "sender" "owner_uid" "fee_receiver_uid" "dex_name" "portal_url" "open_mode" maker_fee_ratio taker_fee_ratio ["fees"] ["memo"]

**Parameters**  

`sender`                 (string required) the tx sender's address    

`owner_uid`            (string, required) the dexoperator 's owner account    

`fee_receiver_uid`  (string, required) the dexoperator 's fee receiver account    

`dex_name`                  (string, required) dex operator's name    

`portal_url`              (string, required) the dex operator's website url    

`open_mode`                 (string, required) indicate the order is PUBLIC or PRIVATE    

`maker_fee_ratio`     (number, required) range is 0 ~ 50000000, 50000000 stand for 50%    

`taker_fee_ratio`     (number, required) range is 0 ~ 50000000, 50000000 stand for 50%    

`order_open_dexop_list` (array of number, required) order open dexop list, max size is 500    

`fee`                       (symbol:fee:unit, optional) tx fee,default is the min fee for the tx type    

`memo`                     (string, optional) dex memo    

**Result**   

`txHash`               (string) The transaction id.    

**Examples**

```
// Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexoperatorregtx "0-1" "0-1" "0-2" "wayki-dex" "http://www.wayki-dex.com" "PRIVATE" 2000000 2000000 '[]'

// Response
{
    "txid" : "1b0e71f49956215dc6782b92c8c26e4714c7a06374fd379831554bc897ebd2d6"
}

As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexoperatorregtx", "params": ["0-1", "0-1", "0-2", "wayki-dex", "http://www.wayki-dex.com", "PRIVATE", 2000000, 2000000, []] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```
---

## GetDEXInfo- getdexoperatorbyowner
get dex operator by dex operator owner.

**Parameters**   

`owner_addr`  (string, required) owner address  

**Result**  
`dex_operator`   detail  

**Examples**
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind getdexoperatorbyowner 0-1

// Response
{
    "id" : 1,
    "owner_regid" : "0-1",
    "owner_addr" : "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4",
    "fee_receiver_regid" : "0-2",
    "fee_receiver_addr" : "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6",
    "name" : "wayki-dex",
    "portal_url" : "http://www.wayki-dex.com",
    "order_open_mode" : "PRIVATE",
    "maker_fee_ratio" : 2000000,
    "taker_fee_ratio" : 2000000,
    "order_open_dexop_list" : [
    ],
    "memo" : "",
    "memo_hex" : "",
    "activated" : false
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getdexoperatorbyowner", "params": [0-1] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

---

## Update DEX Config- submitdexopupdatetx

submitdexopupdatetx  "sender" "dex_id" "update_field" "value" "fee"  

**Parameters**   

`sender`                (string, required)  the tx sender, must be the dexoperaor's owner regid  

`dex_id`                (number, required) dex operator's id   

`update_field`    (nuber, required) the dexoperator field to update  
```
1: owner_regid (string) the dexoperator 's owner account  
2: fee_receiver_regid: (string) the dexoperator 's fee receiver account
3: dex_name: (string) dex operator's name
4: portal_url: (string) the dex operator's website url
5: open_mode: (string) indicate the order is PUBLIC or PRIVATE  
6: maker_fee_ratio: (number) range is 0 ~ 50000000, 50000000 stand for 50%  
7: taker_fee_ratio (number) range is 0 ~ 50000000, 50000000 stand for 50%  
8: order_open_devop_list (Array of number) order open dexop list, max size is 500  
9: memo
```

`value`          (string, required) updated value   

`fee`              (symbol:fee:unit, optional)  tx fee,default is the min fee for the tx type  

**Result**    
`txHash`        (string) The transaction id.  

**Examples**
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexopupdatetx 0-1 1 3 test

// Respond
{
    "txid" : "59c9a3248825c593c3345f3ac6dfc3a31635fbccf2b2e5eda22e739d9fcc8e85"
}

//As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexopupdatetx", "params": [0-1 1 3 test] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

---
## Activate The DEX

### submitdexswitchproposal  
create proposal about enable/disable dexoperator

**Parameters**    
submitdexswitchproposal "sender" "dexid" "operate_type" ["fee"]

`sender`                (string,     required)   the tx sender's address  
`dexid`                  (numberic,   required) the dexoperator's id  
`operate_type`    (numberic,   required) the operate type  
```
1 stand for enable
2 stand for disable
```
`fee`                   (combomoney, optional) the tx fee

**Examples**  
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexswitchproposal 0-1 1 1

// Respond
{
    "txid" : "6df685190033396a34ae02a043601417fba2c9f52a0657f432940ee0504de017"
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexswitchproposal", "params": ["0-1", 1 ,1, "WICC:1:WI"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```  

**attention：DEX going to be activated after Governance Committee of waykichain review**

---
