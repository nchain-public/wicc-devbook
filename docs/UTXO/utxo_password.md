#  UTXO password lock

## Introduce
**Unlock password lock**  
```
cond_type:13  
{
password: 
}

conds content：[{\"cond_type\": 13, \"password\":\"123456\"}]
```

**set password lock**
```
cond_type:113 password lock
{
    password_proof_required: Deterministic uniqueness; flase：everybody can use it if know password； true：you can use password to get it after use interface `submitpasswordprooftx`  to proof.
    password:the length of password has not limit, but more then 256, there will can't prase the password
}

conds content：[{\"cond_type\":113,\"password_proof_required\":true,\"password\":\"123456\"}]
```

<br>

## proof-submitpasswordprooftx  
you need use it if password_proof_required is true.    
function :
To set up an account can use it, then other person can't use coin even if haven password. Extremely safe

---
**Arguments**  

`sender`:               (string, required) the addr submit this tx  

`prev_utxo_txid`:        (string, required) The utxo txid you want to spend    

`prev_utxo_vout_index`: (string, required) The index of utxo output  

`password`:             (symbol:amount:unit, required) password    

`pre_utxo_tx_uid`:       (string, required) the txUid of prev utxotx that provide prevOutput  

`symbol:fee:unit`:      (symbol:amount:unit, optinal) fee paid to miner  

**Result**

`txid`                   (string) The transaction id.  

**example**
```
> ./coind submitpasswordprooftx "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4" "23ewf90203ew000ds0lwsdpoxewdokwesdxcoekdleds" 5 "123" '0-2" "WICC:10000:sawi"

As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitpasswordprooftx", "params": ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", "23ewf90203ew000ds0lwsdpoxewdokwesdxcoekdleds", 5, "123", "0-2", "WICC:10000:sawi"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

## simple process

you can ignore `proof` step if PasswordHashLockCondOut is set false.  
```
// charge money
root@0dc1d946424b:/opt/wicc# coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[]"  "[{\"coin_amount\":100,\"conds\":[{\"cond_type\":113,\"password_proof_required\":true,\"password\":\"123456\"}]}]" "WICC:100000:sawi" "password test"
{
    "txid" : "b8e8d643af36b2ed77d8440b46d4ff95d2a073d07e05ce1614bd34faaf37f092"
}

// proof
**proof** set only address `"0-2"` can get
root@0dc1d946424b:/opt/wicc# coind submitpasswordprooftx "0-2" "b8e8d643af36b2ed77d8440b46d4ff95d2a073d07e05ce1614bd34faaf37f092" 0 "123456" "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ"
{
    "txid" : "1a89856c40bdb4dcd611049ec0a8ff80d7cee1dd2c72495462034bd8ead66eb2"
}

// draw money
root@0dc1d946424b:/opt/wicc# coind submitutxotransfertx "0-2" "WICC" "[{\"prev_utxo_txid\":\"b8e8d643af36b2ed77d8440b46d4ff95d2a073d07e05ce1614bd34faaf37f092\",\"prev_utxo_vout_index\":0,\"conds\":[{\"cond_type\": 13, \"password\":\"123456\"}]}]" "[]" "WICC:1000000:sawi" "parse password"
{
    "txid" : "28bda7d043b3828da4b90ff3fef52850e1766f6229cf929f31238afd676a37d5"
}
```