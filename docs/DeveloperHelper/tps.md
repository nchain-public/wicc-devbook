### How to run a TPS test using JSON RPC

First you need to create a node. Using Docker is the recommended method.
See the Docker section to get started then come back here.

---

<br>

#### Starting the TPS test

**NOTE:** this is for RegTest only. `startcommontpstest` can't be used with TestNet or MainNet.

**Start the TPS test**
```
coind startcommontpstest 20 50
```

**Results**
```json
{
    "msg" : "success"
}
```

To see the happenings of the test. We need to follow the INFO.log file which is located in `/root/.WaykiChain/regtest/INFO.log`

Tail the INFO.log file
```
tail -f /root/.WaykiChain/regtest/INFO.log
```
**Results**
```log
2019-09-29 05:07:56 [miner/miner.cpp:559]INFO: CreateNewBlockStableCoinRelease() : height=24539, tx=5000, totalBlockSize=231313
```

<br>

#### **Stop the TPS test**
**Note:** If we dont stop the test it will continue running forever. 
```
coind startcommontpstest 0 0
```

**Results**
```json
{
    "msg" : "success"
}
```

<br>

##### Calculating TPS
So after running the test for 10 minutes you can read the log and check out the transactions (tx=2600). To calculate the average you take 1 block and divide it by 3 seconds. 

**Why do we divide by 3 seconds?**

1) Every 3 seconds 1 block is produced with corresponding transactions.   

**TPS calculation example**
```
tx=5000 / 3 secs  = 1666 TPS
```

<br>

---

























