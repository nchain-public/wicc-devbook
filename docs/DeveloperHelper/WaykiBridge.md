<extoc></extoc>
# WaykiBridge

## Introduction
`WaykiBridge` is a DAPP development wallet interface tool launched by the wiki chain development team. Developers can integrate `WaykiBridge` when developing DAPP, that is, just call a set of front-end code (H5) to call `WaykiMax` wallet and [Wiki era APP] (https://www.waykichain.com/WaykiTimes.html) wallet for signing and broadcasting WaykiChain transactions so that users can access the 'Browser' and [Wiki Times App] (https://www.waykichain.com/ WaykiTimes.html) Use DAPP in both the Application Center.


## Tools Download

### [Web Wallet Plugin WaykiMax Download] (./webextension.md)

### wiki era app developer version (Testnet) download
Please use the mobile browser to download from the following link

**IOS:** [https://www.pgyer.com/4OIc](https://www.pgyer.com/4OIc )

**Android:** [https://wicc-dev-mix.oss-cn-shenzhen.aliyuncs.com/%E7%BB%B4%E5%9F%BA%E6%97%B6%E4%BB% A3/local/WaykiTimes_2.1.1_production_release_dev.apk](https://wicc-dev-mix.oss-cn-shenzhen.aliyuncs.com/%E7%BB%B4%E5%9F%BA%E6%97%B6 %E4%BB%A3/local/WaykiTimes_2.1.1_production_release_dev.apk)

### wiki era app official version (Mainnet) download
Please use the mobile browser to scan the QR code below to download

![](../images/wikiTimes.png)

### How do developers add test links using the Wiki Age app?

#### 1, [Open Wiki Age]-> [My]->[Settings]->[Open Developer Mode]
#### 2, [Open the wiki era] -> [Application] -> [As shown below]
![](../images/devtest1.png)

## WaykiBridge call example

** Sample link: ** [https://waykibridge-testpage.wiccdev.org/](https://waykibridge-testpage.wiccdev.org/)

**Source: ** [https://gitlab.com/waykichain-public/wicc-dapps/wicc-appcenter-test#](https://gitlab.com/waykichain-public/wicc-dapps/wicc-appcenter -test#)

## WaykiBridge API Description

### Ready to work
#### Introduced the following js
```
<script src="https://sw91.net/devops/js/waykiBridge.js"></script>
```

**Note:** If you need to call the interface when the page is loaded, please do the delay processing after importing the file:

```(setTimeout(() => this.getAddressInfo(), 100);)```

---
### Calling format

#### waykiBridge.walletPlugin(interface name, interface parameter, successful callback, failure callback)


```javascript
waykiBridge.walletPlugin(name,query,function(res){},function(err){})
```
**Note 1: **The attributes in the query are all strings, except for arrays.

**Note 2:** The unit of entry is `sawi`

---

### API error code description

| errorCode (Int) | errorMsg (String) | Meaning |
| :------- | :------- | :------- |
| 0 | \ | Operation success |
| 1000 | Please install WaykiMax first. | No wallet installed |
| 2000 | Please create or import wallet first. |Do not create or import wallet|
| 7000 | User denied transaction signature. | User rejects signature request |

**Note: ** Developers only need to make state judgments based on ``errorCode``, and `errorMsg` is only used for status information prompts.

---

### getAddressInfo(Get User Information)
Query parameter
```
{} //No parameters, please empty the object
```
Example
```
    waykiBridge.walletPlugin(
        "getAddressInfo",
        {},
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
    );
```
Successful callback
```
{
    "result": {
        "network": "mainnet", // "mainnet": main network: testnet test network
        "address": "WPqY8RJHN1u4Kzrnj2mHG9V8igJJVVcveb",
        "regid": "0-1" // the corresponding regid of the account, empty if it does not exist
    },
    "errorCode": 0
}
```

---

### walletPluginUCoinTransfer(Multi-currency transfer)
Query parameter
```
{
   "destArr":[
    {
        "amount": "100000000", //transfer amount, type: String, unit: `sawi`
        "coinSymbol": "WICC", //Currency type, supported but not limited to WICC WUSD WGRT
        "destAddr": "wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq" //Receipt address
    }
],
    "memo": "test transfer" //Remarks, winding
}

```
Example
```
waykiBridge.walletPlugin(
        "walletPluginUCoinTransfer",
        {
            "destArr":[
                {
                "amount": "100000000",
                "coinSymbol": "WICC",
                "destAddr":"wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq"
                }
            ],
            "memo": "test transfer"
        },
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
);

```
Successful callback
```
{
    "result": {
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d"
      },
    "errorCode": 0
}
```

---

### walletPluginUContractInvoke(Multi-currency contract call)
Query parameter
```
{
      "amount": "100000000", //the amount transferred to the contract when the contract is called, type: String, unit: `sawi`
      "coinSymbol": "WICC", //Currency type, supported but not limited to WICC WUSD
      "regId": "0-1", //contract regid
      "contract": "f001", //contract field
      "memo":"test transfer" //Remarks, not on the chain
}

```
Example
```
waykiBridge.walletPlugin(
        "walletPluginUContractInvoke",
        {
          "amount": "100000000",
          "coinSymbol": "WICC",
         "regId": "0-1",
         "contract": "f001",
         "memo": "test transfer"
        },
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
);
```
Successful callback
```
{
    "result": {
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d"
      },
    "errorCode": 0
}
```


### walletPluginContractIssue (contract release)
Query parameter
```
{
    "contractContent": "", //Contract content
    "contractDesc": "" //Contract description
}
```
Example
```
    waykiBridge.walletPlugin(
        "walletPluginContractIssue",
        {
          contractContent: 'mylib = require "mylib"',
          contractDesc: "description",
        },
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
    );
```
Successful callback
```
{
    "result": {
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d"
      },
    "errorCode": 0
}
```

### walletPluginAss
etIssue (issuing WRC30 token assets)
Need to pay 550 WICC as the issue fee

Query parameter
```
{
   "assetSymbol": "ABCDEFG", //asset symbol 6~7 [A-Z] characters
   "assetName": "STOKEN", // asset name 1~32 random characters
   "assetSupply": "100000000000", //asset circulation, maximum: 900*10^8*10^8 unit `sawi`
   "assetOwnerId": "wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq", // Asset Owner Address
   "assetMintable": false, //Can the asset be appended with `true` or `false`
}

```
Example
```
waykiBridge.walletPlugin(
        "walletPluginAssetIssue",
        {
      "assetSymbol": "ABCDEFG", //asset symbol
      "assetName": "STOKEN", // asset name
      "assetSupply": "100000000000", //asset circulation
      "assetOwnerId": "wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq", // Asset Owner Address
       "assetMintable": false, / / ​​whether the asset can be issued
    },
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
);
```
Successful callback
```
{
    "result": {
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d"
      },
    "errorCode": 0
}
```

### walletPluginAssetUpdate (Update WRC30 token assets, not yet enabled)
Only the owner, asset name, and asset issuance of the asset are allowed to be updated, and 110 WICC is required for each update fee.

Query parameter
```
{
    "assetSymbol": "ABCDEFG", //requires updated asset symbol
    "updateType": "1", //update type "1": asset owner, "2": asset name, "3": asset issue amount
    "updateValue": "wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq", //The minimum number of asset issuance is 100000000, the unit is `sawi`
}
```
Example
```
waykiBridge.walletPlugin(
     "walletPluginAssetUpdate",
        {
            "assetSymbol": "STOKEN", //asset symbol
            "updateType": "1"
            "updateValue": "wbZTWpEnbYoYsedMm2knnP4q7KiSdS3yVq", // asset owner address
         },
        Function(res) {
            Console.log(res)
        },
        Function(err) {
            Console.log(err)
        }
);

```
Successful callback
```
{
    "result": {
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d"
      },
    "errorCode": 0
}
```
