# UTXO Basic exchange

**Acction：The input of each stroke is the output of the previous stroke. you can easier understand the rule of input and output if you understand this word!**

---
## submitutxotransfertx
Submit utxo tx

**Parameters**  

`sender`                (string, required) The addr submit this tx 

`coin_symbol`      (string, required) The utxo transfer coin symbole

`vins`             (string(json), required) The utxo inputs   

`vouts`            (string(json), required) The utxo outputs  

`symbol:fee:unit`   (symbol:amount:unit, optional) Fee paid to miner  

`memo`              (string,optinal) Tx memo  

**Result**  

`txid`                 (string) The transaction id.

**Judge state of exchange by the value of vins and vouts.**  

|state|vins|vouts|
|----|----|----|
|charge money|empty|exist|
|spend money|exist|exist|
|draw money|exist|empty|


**detail of vins**  
```
1、prev_utxo_txid: The tx hash of previous utxo  
2、prev_utxo_vout_index: The index of the output with previous utxo exchange
3、conds:the conditions of unlock utxo
{
cond_type:11  Unlock the address lock
cond_type:12  Unlock the multi-signlock
cond_type:13  Unlock the password lock
{
password: 
}
cond_type:14  Unlock over time lock, reach the specified height of type 114 UTXO
cond_type:15  Unlock recycle lock，reach the specified height of type 115 UTXO
}

```

Take solving password locks as an example, vins content is as follow:

```
"[{\"prev_utxo_txid\":\"9d36b478cce47ae3429e6b58ec97e8c54e0764deaff26b2adcd9d37bbc2cb80b\",\"prev_utxo_vout_index\":0,\"conds\":[{\"cond_type\": 13, \"password\": \"123456\"}]"
```


**detail of vouts**  

```
1、`coin_amount`: number of turn out coin
2、`conds`: lock utxo, it's unlock state if content is empty, and exist differ param depend on type
    {
    cond_type:111   single lock, address lock
    {
        uid:
    }
    
    cond_type:112   multi-sign lock
    {
        multisign_address: 
    }

    cond_type:113   password lock
    {
        password_proof_required: Deterministic uniqueness; flase：everybody can use it if know password； true：you can use password to get it after use interface `submitpasswordprooftx`  to proof.
        password:the length of password has not limit, but more then 256, there will can't prase the password
    }
    
    cond_type:114   over time lock
    {
        height: the height of block
    }
    
    cond_type:115   recycle lock
    {
        height: the height of block
    }
}

```

Take solving single locks as an example, vouts content is as follow:
```
[{"coin_amount":10000,"conds":[{"cond_type":111,"uid":"0-1"}]
```


**Examples**  
```
> ./coind submitutxotransfertx "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4" "WICC" "[]"  "[]" "WICC:10000:sawi" "xx"

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitutxotransfertx", "params": ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", "WICC", "[]", "[]", "WICC:10000:sawi", "xx"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```
----

### charge money 
user tansfer to utxo from balance  
vins is empty, vout exist   

---

#### 
**example**
```
// Request
coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[]"  "[{\"coin_amount\":10000,\"conds\":[]}]" "WICC:100000:sawi" "xx"

// Respond
{
    "txid" : "cdb2250416487409acd21b4d98f322de9aab5cd7265432c13536c33f4a909eb4"
}
```
#### address lock
**example**
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind submitutxotransfertx 0-1 "WICC" "[]" '[{"coin_amount":10000,"conds":[{"cond_type":111,"uid":"0-1"}]}]' "WICC:100000:sawi" "test"

// Respond
{
    "txid" : "be685d611ac914b41138ffaa68c87ac3b177c4fb82a7c03bab013fe34452b0f4"
}
```

#### password lock
**example**
```
// Request
root@0dc1d946424b:/opt/wicc# coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[]"  "[{\"coin_amount\":100,\"conds\":[{\"cond_type\":113,\"password_proof_required\":true,\"password\":\"123456\"}]}]" "WICC:100000:sawi" "password test"

// Respond
{
    "txid" : "6d8c07ae4ea1868e2cb7d458b636acc0204c9a7637036fd7b02e62625b73a01c"
}
```

### spend money  
vins exit, vout exit

---

#### unlock to unlock
**example**
```
// Request
coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[{\"prev_utxo_txid\":\"be685d611ac914b41138ffaa68c87ac3b177c4fb82a7c03bab013fe34452b0f4\",\"prev_utxo_vout_index\":0,\"conds\":[]}]" "[{\"coin_amount\":100000,\"conds\":[]}]" "WICC:1000000:sawi" "spend utxo"

// Response
{
    "txid" : "307d44227137eb47006479a8b02c86dd63e995fb024cbcf6aa9c342f36ea4f5c"
}
roo

```

#### unlock to single lock  
**example**
```
// Request
root@0dc1d946424b:/opt/wicc# coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[{\"prev_utxo_txid\":\"307d44227137eb47006479a8b02c86dd63e995fb024cbcf6aa9c342f36ea4f5c\",\"prev_utxo_vout_index\":0,\"conds\":[]}]" "[{\"coin_amount\":100000,\"conds\":[{\"cond_type\":111,\"uid\":\"0-1\"}]}]}]" "WICC:1000000:sawi" "spend utxo"

// Respond
{
    "txid" : "9d36b478cce47ae3429e6b58ec97e8c54e0764deaff26b2adcd9d37bbc2cb80b"
}
```

coind submitutxotransfertx "wQ3FinggSoJU7x27J2cdfao9rXdZxCNqoQ" "WICC" "[]"  "[{\"coin_amount\":100,"\coinds\":[\"cond_type\":113,\"password_proof_required\":False,\"password\":123456]]" "WICC:100000:sawi" "password in"

### draw money
vins empty, vout exit

---
#### unlock password lock
**example**  
```
coind submitutxotransfertx "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4" "WICC" "[{\"prev_utxo_txid\":\"6d8c07ae4ea1868e2cb7d458b636acc0204c9a7637036fd7b02e62625b73a01c\",\"prev_utxo_vout_index\":0,\"conds\":[{\"cond_type\": 13, \"password\":\"123456\"}]}]" "[]" "WICC:1000000:sawi" "parse password"
```