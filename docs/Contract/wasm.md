# Hello world  

## Compile Project

**hello content**  
include/hello.hpp
```
#include <wasm.hpp>
using namespace wasm;
using namespace std;

CONTRACT hello : public contract {
   public:
      using contract::contract;

      ACTION hi( name nm );

};
```
src/hello.cpp   
```
#include <hello.hpp>

ACTION hello::hi( name nm ) {
   print_f("Name : %\n", nm);
}

WASM_DISPATCH( hello, (hi))
```
**Detail of file levels and directories refer to [wicc-wasm-cdt/examples/hello](https://github.com/WaykiChain/wicc-wasm-cdt/tree/master/examples/hello)**  

**Note:**  The naming convention for ACTION Name is
```
Maximum 12 characters, can only be composed of lowercase letters, Numbers 1-5, "."
```

**input**  
Build wasm from c++ project:
```
$ cd examples/hello
$ mkdir build
$ cd build
$ cmake ..
$ make
```

**result**
```
[100%] Built target hello_project
➜  build git:(master) ll
total 56
-rw-r--r--   1 hpy  staff    14K  7  6 16:46 CMakeCache.txt
drwxr-xr-x  14 hpy  staff   448B  7  6 16:46 CMakeFiles
-rw-r--r--   1 hpy  staff   4.6K  7  6 16:46 Makefile
-rw-r--r--   1 hpy  staff   1.4K  7  6 16:46 cmake_install.cmake
drwxr-xr-x   8 hpy  staff   256B  7  6 16:46 hello
drwxr-xr-x   4 hpy  staff   128B  7  6 16:46 hello_project-prefix
➜  build git:(master) cd hello
➜  hello git:(master) ll
total 72
-rw-r--r--   1 hpy  staff    14K  7  6 16:46 CMakeCache.txt
drwxr-xr-x  13 hpy  staff   416B  7  6 16:46 CMakeFiles
-rw-r--r--   1 hpy  staff   5.2K  7  6 16:46 Makefile
-rw-r--r--   1 hpy  staff   1.4K  7  6 16:46 cmake_install.cmake
-rw-r--r--   1 hpy  staff   1.0K  7  6 16:46 hello.abi
-rwxr-xr-x   1 hpy  staff   955B  7  6 16:46 hello.wasm
```
If the hello.abi and hello.wasm files exist, then prove compile successfully。Copy them to where you want them

---
<br>

**The following is a simple process for the contract deployment invocation, Refer to[Smart Contract-wasm](../JsonRpcApi/contract_wasm.md)**  

## Deploy a contract
---
Using interface:  `submitsetcodetx`

```
// Request
coind submitsetcodetx 0-2 /tmp/wasm/hello/hello.wasm /tmp/wasm/hello/hello.abi


// Response
"txid" : "3e5f9f31cdcf32ff93f4f8741b1fc467721b93a0282e7f5bea758096ffb49068"
```

## Get regid of smart contract
---
Using interface:  `getcontractregid`
```
// Request
coind getcontractregid "3e5f9f31cdcf32ff93f4f8741b1fc467721b93a0282e7f5bea758096ffb49068"


// Response
{
    "regid" : "9925-2",
    "regid_hex" : "c52600000200"
}
```


## Call the contract
---
using interface: `submittx`   
```
// Request
coind submittx 0-2 9925-2 hi '["kj"]'


// Response
{
    "txid" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
    "tx_trace" : {
        "trx_id" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
        "elapsed" : 262,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
                "receiver" : "9925-2",
                "trx" : {
                    "contract" : "9925-2",
                    "action" : "hi",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "nm" : "kj"
                    }
                }
            }
        ]
    }
}
```
