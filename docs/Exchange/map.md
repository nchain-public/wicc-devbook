# ERC20-to-Mainnet Swapping
#### WaykiChain ERC20 to mainnet Token Swapping Description

#### WaykiChain official WICC (mainnet WICC) and exchange WICC(ERC20) replacement method

1. The exchange transfers the ETC20' WICC to the destruction address `0x0000000000000000000000000000000000000000` for destruction (the amount of coins is less than 500,000), or transfers to the official address which provided by WaykiChain Officially.
2. The exchange provides a WaykiChain mainnet address.
3. The WaykiChain officially transfers the same amount of WICC (mainnet WICC) to the WaykiChain mainnet address which provided by the exchange.
