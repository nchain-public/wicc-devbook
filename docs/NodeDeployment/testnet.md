### Setting up Docker and Linux (Ubuntu / CentOS) 
We go through how to use Docker and Linux to run a node on Testnet. Lets start with Docker

<br>

---
### Docker
---

<br>

Refer to [build for docker][1] And [WaykiChain.conf][2]

### The local directory `/opt/wicc` is should be structured as follows. 
```
~/workspace/wicc/WaykiChain_testnet$tree
.
├── bin
│   └── run-waykicoind-test.sh
└── conf
    └── WaykiChain.conf

```

#### Startup script content `run-waykicoind-test.sh`
```shell
docker run --name waykicoind-testnet -p 18920:18920 -p 6967:6968 \
  -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
  -v `pwd`/data:/root/.WaykiChain/testnet \
  -v `pwd`/bin:/opt/wicc/bin \
  -v `pwd`/lua:/tmp/lua \
  -v `pwd`/wasm:/tmp/wasm \
  -d wicc/waykicoind:3.2
```

#### Configuration file content `WaykiChain.conf`
```
#nettype=main|test|regtest
nettype=test
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
logprinttofile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

### Start node

## ```sh bin/run-waykicoind-test.sh```

**example**

```
\~/workspace/wicc/WaykiChain_testnet$sh bin/run-waykicoind-test.sh
284cb238ac8a0aa737d1c6c9033358d464c9a274f669e8c79acfc3776f4e6984
\~/workspace/wicc/WaykiChain_testnet$docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                                                 NAMES
284cb238ac8a        wicc/waykicoind     "./coind"           6 seconds ago       Up 5 seconds        8920/tcp, 0.0.0.0:6967-\>6968/tcp, 0.0.0.0:18920-\>18920/tcp           waykicoind-testnet
\~/workspace/wicc/WaykiChain_testnet$
```

### Check if the node is working properly

## ```docker exec -it waykicoind-testnet /bin/bash```

**example**

```
\~/workspace/wicc/WaykiChain_testnet$docker exec -it 284cb238ac8a /bin/bash
root@284cb238ac8a:/opt/wicc# coind getinfo
{
    "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "TEST_NET",
    "proxy" : "",
    "public_ip" : "183.239.240.22",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/testnet",
    "block_interval" : 3,
    "genblock" : 0,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 2,
    "tipblock_fuel_rate" : 1,
    "tipblock_fuel" : 0,
    "tipblock_time" : 1570535136,
    "tipblock_hash" : "f99c82071e02556e1c462231121d0a10d702d02a4dace148abf1240a871e5a3f",
    "tipblock_height" : 8806,
    "synblock_height" : 6401676,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 1,
    "errors" : "",
    "state" : "IBD"
}
root@284cb238ac8a:/opt/wicc#
```
If the return value is the same as above, the Testnet node is successfully built.

<br>

---
## Linux (Ubuntu/CentOS)
---

<br>

In this section we go over how to install WaykiChain on Linux (Ubuntu/CentOS)

Refer to [build locally](./build.md) And [WaykiChain.conf](./conf.md)

#### The local directory `/opt/wicc` is structured as follows
```
root@ubuntu:/opt/wicc# ls
coind  WaykiChain
```

#### The local directory `~/.WaykiChain ` is structured as follows
```
root@ubuntu:/opt/wicc# ls ~/.WaykiChain
 WaykiChain.conf
 ```

#### Configuration file content`WaykiChain.conf`
```
#nettype=main|test|regtest
nettype=test
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
logprinttofile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

#### Start node

command
```
./coind &
```


#### Check if the node is working properly

After the node starts, run the following command in a terminal window to confirm whether the node is running normally.
```
./coind getinfo
```

**example**

```
root@ubuntu:/opt/wicc# ./coind getinfo
{
    "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "TEST_NET",
    "proxy" : "",
    "public_ip" : "183.239.240.22",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/testnet",
    "block_interval" : 3,
    "genblock" : 0,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 2,
    "tipblock_fuel_rate" : 1,
    "tipblock_fuel" : 0,
    "tipblock_time" : 1570535136,
    "tipblock_hash" : "f99c82071e02556e1c462231121d0a10d702d02a4dace148abf1240a871e5a3f",
    "tipblock_height" : 8806,
    "synblock_height" : 6401676,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 1,
    "errors" : "",
    "state" : "IBD"
}
root@ubuntu:/opt/wicc#
```

If the return value is the same as above, the Testnet node is successfully built.

[1]:	./build.md
[2]:	./conf.md

<br>

---
