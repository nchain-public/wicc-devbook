### Running WaykiChain in Linux

We also support using WaykiChain natively on Linux for those who are not familiar with Docker.
Here we will go through how to run WaykiChain coind with Linux

---

<br>

**Supported Systems**

**For now, the supported Linux 64bit systems, are：**

1. Ubuntu 18.x
2. Cent OS 7.x/Cent OS 8.x

**Note:** Other Linux builds are untested   
It is recommended to use GCC version 7.5 or above

---

<br>

### Prepare compilation environment ready   

```shell
sudo add-apt-repository ppa:bitcoin/bitcoin -y 
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes build-essential libtool autotools-dev automake \
    pkg-config libssl-dev libevent-dev bsdmainutils python3 \
    libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-program-options-dev \
    libboost-test-dev libboost-thread-dev software-properties-common libdb4.8-dev libdb4.8++-dev git-core
```

### Automated script

we are going to prepare an automated download script

The script `build_local.sh`content is as follows. 

```shell
#!/bin/bash
sudo mkdir -p /opt/wicc
cd /opt/wicc

#build waykicoind
sudo curl https://raw.githubusercontent.com/WaykiChain/WaykiChain/rel/v3.2.1/distros/linuxshell/prepare_prerequisites.sh|bash 
sudo rm -rf WaykiChain
git clone --recurse-submodules -b rel/v3.2.1  https://github.com/WaykiChain/WaykiChain.git
cd ./WaykiChain/distros/linuxshell
sh linux.sh
cd ../../
sh autogen-coin-man.sh coin
make

#copy the coind and WaykiChain.conf
cd ..
cp ./WaykiChain/src/coind coind

#to create directory ~/.WaykiChain if it not exist.
mkdir ~/.WaykiChain

cp ./WaykiChain/docker/WaykiChain.conf ~/.WaykiChain/WaykiChain.conf
```

<br>

### Download and compile
Run the following command to execute the script to download the source code.

```sh build_local.sh```

<br>

### Check
* please check if have **coind** (executable file) is in the directory `/opt/wicc/` and **WaykiChain.conf** (config file) is in the directory ``

**example**
```
root@ubuntu:/opt/wicc# ls
coind  WaykiChain
root@ubuntu:/opt/wicc\#ls ~/.WaykiChain
WaykiChain.conf
```

**Now goto WaykiChain src directory and run `./coind`**
```
./coind &
```

**check if node running normal**
```
./coind getinfo
```
**result**
```json
{
     "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "REGTEST_NET",
    "proxy" : "",
    "public_ip" : "",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/regtest",
    "block_interval" : 10,
    "genblock" : 1,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 3,
    "tipblock_fuel_rate" : 100,
    "tipblock_fuel" : 0,
    "tipblock_time" : 1504305600,
    "tipblock_hash" : "ab8d8b1d11784098108df399b247a0b80049de26af1b9c775d550228351c768d",
    "tipblock_height" : 0,
    "synblock_height" : 0,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 0,
    "errors" : "",
    "state" : "IBD"
}
```

**Configuration**
```
vim ~/.WaykiChain/WaykiChain.conf
```

**TestNet Example conf**
```
#nettype=main|test|regtest
nettype=test
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
debuglogfile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=1
genblocklimit=1000000
debug=INFO
```

[Github Readme](https://github.com/WaykiChain/WaykiChain/wiki/How-to-build-WaykiChain-core)

<br>



Please start to deploy to regTest , testnet, or mainnet for your development.

<br>

---
