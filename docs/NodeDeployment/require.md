# Node Requirements
#### Node Deployment Environment Requirements

#### Software Requirements ：
* Deployment Local：support Linux-like 64bit systems, such as Ubuntu 14.x/Ubuntu 16.x/Ubuntu 18.x/Cent OS 7.x only.
* By Docker：Linux、Mac OSX any version

---


#### Hardware Requirements：

##### General Nodes：
* CPU:  \>= 2 Core
* RAM:  \>= 8 GB
* hard disk: \>= 100G\*\*(Recommend to use 100GB hard drive even though
 the download of all blocks only need 20GB space currently)\*\*
* Network bandwidth output： \>= 2 Mbps
* Network bandwidth input：\>= 2 Mbps

##### Block producer Nodes：
* CPU: 8 cores
* RAM: 12 GB
* hard disk: \>= 100G \*\*(Recommend to use 100GB hard drive even though
 the download of all blocks only need 20GB space currently)\*\*
* Network bandwidth output： \>= 10 Mbps
* Network bandwidth input：  \>= 10 Mbps

**Increase Virtual RAM for CentOS, the commands are as follows:**
``` shell
sudo dd if=/dev/zero of=/mnt/swapfile bs=1M count=4096
sudo mkswap /mnt/swapfile
sudo chown root. /mnt/swapfile
sudo chmod 0600 /mnt/swapfile
sudo swapon /mnt/swapfile
sudo sh -c 'echo "/mnt/swapfile none swap sw 00" >> /etc/fstab'
```
**attention: count is the size of the added memory, 4096=4G**
